package com.example.projet_movie_database;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilmList {

    @SerializedName("total_pages")
    int totalpages=0;

    @SerializedName("results")
    List<Film> films;

    public int getTotalpages() {
        return totalpages;
    }

    public void setTotalpages(int totalpages) {
        this.totalpages = totalpages;
    }

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }

    public List<Film> getResults() {
        return films;
    }

    public void setResults(List<Film> results) {
        this.films = results;
    }

}
